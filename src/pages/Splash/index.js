import LottieView from 'lottie-react-native';
import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {colors} from '../../utils';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('WelcomeAuth');
    }, 3000);
  });
  return (
    <View style={styles.wrapper.page}>
      <LottieView
        style={styles.wrapper.illustration}
        source={require('../../assets/Illustrations/socialmedia.json')}
        autoPlay
        loop
      />
      <Text style={styles.wrapper.text.title}>BEWEI AGENCY</Text>
    </View>
  );
};

const styles = {
  wrapper: {
    page: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'white',
      flex: 1,
    },
    illustration: {
      width: 219,
      height: 200,
      marginBottom: 10,
    },
    text: {
      title: {
        fontSize: 24,
        fontWeight: 'bold',
        color: colors.default,
        marginBottom: 76,
      },
    },
  },
};

export default Splash;
