import React from 'react';
import {View, Text, Image} from 'react-native';
import {colors} from '../../utils';

import {welcomeAuth} from '../../assets';
import ActionButton from './ActionButton';

const WelcomeAuth = ({navigation}) => {
  const handleGoTo = screen => {
    navigation.navigate(screen);
  };
  return (
    <View style={styles.wrapper.page}>
      <Image source={welcomeAuth} style={styles.wrapper.illustration} />
      <Text style={styles.wrapper.text.welcome}>BEWEI Agency</Text>
      <Text style={styles.wrapper.text.title}>Romi Firdaus Lazuardi</Text>
      <ActionButton
        desc="Silahkan masuk, jika sudah memiliki akun"
        title="Masuk"
        onPress={() => handleGoTo('Login')}
      />
      <ActionButton
        desc="atau silahkan daftar jika anda belum memiliki akun"
        title="Daftar"
        onPress={() => handleGoTo('Register')}
      />
    </View>
  );
};

const styles = {
  wrapper: {
    page: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'white',
      flex: 1,
    },
    illustration: {
      width: 219,
      height: 200,
      marginBottom: 10,
    },
    text: {
      welcome: {
        fontSize: 24,
        fontWeight: 'bold',
        color: colors.default,
      },
      title: {
        fontSize: 12,
        fontWeight: 'bold',
        color: colors.default,
        marginBottom: 30,
      },
    },
  },
};

export default WelcomeAuth;
