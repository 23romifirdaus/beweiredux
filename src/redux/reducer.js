import {combineReducers} from 'redux';

const initialState = {
  name: 'Romi Firdaus',
};

const initialStateRegister = {
  form: {
    fullName: '',
    email: '',
    password: '',
  },
  desc: 'ini adalah desc untuk register',
  title: 'Register Page',
};

const RegisterReducer = (state = initialStateRegister, action) => {
  if (action.type === 'SET_TITLE') {
    return {
      ...state,
      title: 'Register ganti title',
    };
  }
  if (action.type === 'SET_FORM') {
    return {
      ...state,
      form: {
        ...state.form,
        [action.inputType]: action.inputValue,
      },
    };
  }
  return state;
};

const initialStateLogin = {
  form: {
    email: '',
    password: '',
  },
  info: 'Tolong masukan password anda',
  isLogin: true,
};

const LoginReducer = (state = initialStateLogin, action) => {
  if (action.type === 'SET_FORM') {
    return {
      ...state,
      form: {
        ...state.form,
        [action.inputType]: action.inputValue,
      },
    };
  }
  return state;
};

const reducer = combineReducers({
  RegisterReducer,
  LoginReducer,
});

export default reducer;
